FROM python:3.7-alpine

WORKDIR /srv
RUN wget https://github.com/ltworf/localslackirc/releases/download/1.5/localslackirc_1.5.orig.tar.gz; \
    tar xfv localslackirc_1.5.orig.tar.gz; \
    rm localslackirc_1.5.orig.tar.gz

RUN pip install -r ./localslackirc/requirements.txt

COPY entrypoint.sh /entrypoint.sh

ENV SLACK_TOKEN ""
ENV ROCKET_CHAT_TOKEN ""
ENV ROCKET_CHAT_URL ""
ENV IRCD_PORT "6667"
ENV IRCD_BIND "0.0.0.0"
ENV AUTOJOIN_CHANNELS "yes"
ENV LOCALSLACKIRC_PATH "/home/localslackirc/.localslackirc"

RUN adduser -D localslackirc
USER localslackirc
ENTRYPOINT ["/entrypoint.sh"]
