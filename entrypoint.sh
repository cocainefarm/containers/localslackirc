#!/bin/sh

ROCKET=""
AUTOJOIN=""

echo "Starting localslackirc..."

if [ ! -f "$LOCALSLACKIRC_PATH" ]; then
  if [ -z "$ROCKET_CHAT_TOKEN" ] && [ -z "$SLACK_TOKEN" ]; then
    echo "Neither \$ROCKET_CHAT_TOKEN nor \$SLACK_TOKEN is set. exiting..."
    exit 1
  elif [ -n "$ROCKET_CHAT_TOKEN" ]; then
    if [ -n "$ROCKET_CHAT_URL" ]; then
      ROCKET="--rc-url $ROCKET_CHAT_URL"
    fi
    echo "\$ROCKET_CHAT_TOKEN was given but no url provided. exiting..."
    exit 1
  elif [ -n "$SLACK_TOKEN" ]; then
    echo "$SLACK_TOKEN" > "$LOCALSLACKIRC_PATH"
  fi
fi

if [ -n "$AUTOJOIN_CHANNELS" ]; then
  AUTOJOIN="-j"
fi

echo "Executing: /srv/localslackirc/irc.py -o -t $LOCALSLACKIRC_PATH $AUTOJOIN -i $IRCD_BIND -p $IRCD_PORT $ROCKET"

/srv/localslackirc/irc.py -o -t "$LOCALSLACKIRC_PATH" $AUTOJOIN -i "$IRCD_BIND" -p "$IRCD_PORT" $ROCKET
