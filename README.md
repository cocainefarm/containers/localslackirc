# localslackirc docker container

A docker container for [localslackirc](https://github.com/ltworf/localslackirc)

## Usage
```
docker run -e SLACK_TOKEN=<your token here> localslackirc:latest
```

### Config
`SLACK_TOKEN`: The slack token to use for login.  
`ROCKET_CHAT_TOKEN`: the rocketchat token to use for login.  
    Disables slack and also requires to set `ROCKET_CHAT_URL`.  
`IRCD_PORT`, `IRCD_IP`: set port and ip for the ircd to bind to in the container.  (Default 6667, 0.0.0.0)
`AUTOJOIN_CHANNELS`: automaticly join all channels you are in. (Default yes)  
`LOCALSLACKIRC_PATH`: path to look for the .localslackirc file for, should you wish to mount one in. (Default /
home/localslackirc/.localslackirc)
